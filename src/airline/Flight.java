/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airline;

/**
 *
 * @author iuabd
 */
public class Flight {
    
    //attributes
    private String number;
    private String date;

    public Flight(String number, String date) {
        this.number = number;
        this.date = date;
    }
    
    //getters and setters
    public String getNumber() {
        return number;
    }
    public String getDate() {
        return date;
    }
}
