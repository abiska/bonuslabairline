/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airline;

/**
 *
 * @author iuabd
 */
public class Airline {
    
    //attributes
    private String name;
    private String shortCode;

    public Airline(String name, String shortCode) {
        this.name = name;
        this.shortCode = shortCode;
    }
    
    //getters
    public String getName() {
        return name;
    }
    public String getShortCode() {
        return shortCode;
    } 
}
