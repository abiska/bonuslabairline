/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airline;

/**
 *
 * @author iuabd
 */
public class Airplane {
    
    //attributes
    private String make;
    private String model;
    private int id;
    private int numOfSeat;

    public Airplane(String make, String model, int id, int numOfSeat) {
        this.make = make;
        this.model = model;
        this.id = id;
        this.numOfSeat = numOfSeat;
    }
    
    //getters
    public String getMake() {
        return make;
    }
    public String getModel() {
        return model;
    }
    public int getId() {
        return id;
    }
    public int getNumOfSeat() {
        return numOfSeat;
    }
}
