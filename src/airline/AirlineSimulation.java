/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package airline;

public class AirlineSimulation {
    public static void main(String[] args) throws Exception {
        
        Airline airline = new Airline("AirCanada", "2022");
        Airport airport = new Airport("ABC123", "Toronto");
        Airplane airplane = new Airplane("AirFactory", "DEF", 456, 100);
        Flight flight = new Flight("69", "April 1, 2022");
        
        System.out.println("Flight date: " + flight.getDate() +
            "\nFlight number: "+ flight.getNumber()+
            "\nAirplane id: " + airplane.getId() +
            "\nAirplane make: " + airplane.getMake() +
            "\nAirplane model: " + airplane.getModel() +
            "\nAirplane " + airplane.getNumOfSeat() +
            "\nAirline name: " + airline.getName() +
            "\nAirline code: " + airline.getShortCode() +
            "\nAirport location: " + airport.getCity() + "\nAirport code: " + airport.getCode());
        
    }
}
