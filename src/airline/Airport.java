/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package airline;

/**
 *
 * @author iuabd
 */
public class Airport {
    
    //attributes
    private String code;
    private String city;

    public Airport(String code, String city) {
        this.code = code;
        this.city = city;
    }
    
    //getters
    public String getCode() {
        return code;
    }
   
    public String getCity() {
        return city;
    }
}
